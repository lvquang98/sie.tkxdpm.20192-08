PHÂN CÔNG CÔNG VIỆC
1. Lê Văn Quang
+, 2 Usecase: Xác nhận đơn hàng cần đặt, Tạo đơn hàng cho site
+, Biểu đồ Usecase tổng quát
+, Từ điển thuật ngữ
+, Biểu đồ thực thể liên kết
+, Cơ sở dữ liệu

2.Vũ Ngọc Xuân
+, 2 Usecase: Thêm đơn hàng cần đặt, Xem đơn hàng cần đặt

3. Phạm Minh Hoàng
+, 5 Usecase: Thêm, sửa, xóa, xem mặt hàng kinh doanh của site, Xác nhận đơn hàng của site
+, Tài liệu đặc tả phụ trợ

4.Đỗ Minh Anh
+, 1 Usecase: Cập nhật danh sách mặt hàng kinh doanh

5. Phạm Đức Long
+, 2 Usecase: Xem đơn hàng lưu kho, Xác nhận đơn hàng lưu kho

6. Lê Đức Thắng
+, 2 Usecase: Sửa đơn hàng cần đặt, Xóa đơn hàng cần đặt

7.Nguyễn Phan Huy
+, 1 Usecase: Cập nhật thông tin vận chuyển của site
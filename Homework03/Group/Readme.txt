Phân công công việc:
1. Lê Văn Quang:
+, Tìm hiểu, vẽ biểu đồ Use case tổng quan
+, Đặc tả 2 ca sử dụng: Xác nhận danh sách mặt hàng cần đặt, Tạo đơn hàng
+, Viết từ điển thuật ngữ
2. Đỗ Minh Anh
+, Đặc tả ca sử dụng: Quản lý danh sách mặt hàng
3. Phạm Đức Long
+, Đặc tả ca sử dụng: Xem đơn hàng đã được xác nhận, Xác nhận đơn hàng
4. Nguyễn Phan Huy
+, Đặc tả ca sử dụng: Cập nhật thông tin vận chuyển
5. Vũ Ngọc Xuân
+, Đặc tả ca sử dụng: Xóa đơn hàng, Tạo đơn hàng
6. Lê Đức Thắng
+, Đặc tả 2 ca sử dụng: Sửa đơn hàng, Đọc đơn hàng
7. Phạm Minh Hoàng
+, Đặc tả 2 ca sử dụng: Tạo/Sửa danh sách mặt hàng kinh doanh, Cập nhật số lượng trong kho của site
+, Viết đặc tả phụ trợ